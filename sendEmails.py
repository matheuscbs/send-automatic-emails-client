import smtplib  # SMTP - Simple Mail Transfer Protocol
import dotenv  # .env file
import os  # operating system
from datetime import datetime
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.interval import IntervalTrigger
# MIME - Multipurpose Internet Mail Extensions
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText  # Standardization of sending emails
from email.mime.base import MIMEBase  # attachments
from email import encoders  # encoding
# For create server and send email
# 1 start the server SMTP
dotenv.load_dotenv(dotenv.find_dotenv())


host = os.getenv("HOST")
port = os.getenv("PORT")
login = os.getenv("LOGIN")
password = os.getenv("PASSWORD")
send_email = os.getenv("SEND_EMAIL")  # Email to send
path_archive = os.getenv("PATH_ARCHIVE")  # path to the file to attach
filename = os.getenv("FILENAME")  # name of the file to attach
# 2 make the email type MIME

scheduler = BlockingScheduler()


@scheduler.scheduled_job('interval', minutes=5)
def send_email_with_attachment():
    try:
        server = smtplib.SMTP(host, port)  # Create server
        server.ehlo()  # Identify the server
        server.starttls()  # Start the encryption
        server.login(login, password)  # Login to server
        body = "This is a test email"
        email_msg = MIMEMultipart()
        email_msg["From"] = login
        email_msg["To"] = send_email
        email_msg["Subject"] = "Automatic Email"
        email_msg.attach(MIMEText(body, "plain"))
        attachment = open(path_archive, "rb")  # read the file in binary mode
        # define the type of file
        attachment_part = MIMEBase("application", "octet-stream")
        # Include the file on attachment part
        attachment_part.set_payload(attachment.read())
        # coding the archive on base 64
        encoders.encode_base64(attachment_part)

        attachment_part.add_header("Content-Disposition", "attachment",
                                   filename=filename)  # reading file of attachment
        attachment.close()  # close the file
        email_msg.attach(attachment_part)  # attach the file
        # 3 Send the email type MIME to the server SMTP
        server.sendmail(email_msg["From"],
                        email_msg["To"], email_msg.as_string())
        print("Email sent")
        server.quit()  # close the server
    except Exception as error:
        print('>>>> An error occurred')
        print(error)


scheduler.start()
