# Send Automatic Emails Client

## Getting started

To start you must first have python installed on your machine.
to download click here: [Python](https://www.python.org/downloads/)

## first activate Virtual Env

Use this command e your shell:

```bash
pip install virtualenv
virtualenv venv
<!-- Windows -->
./venv/Scripts/Activate
<!-- Linux/OS -->
source venv/bin/activate
```

## Second install packages

```bash
pip install -r requirements.txt
```

## Environment Variables

To properly run the API with custom configurations, you should create a file named `.env` inside the API folder and add the following environment variables:

- **HOST** - "smtp.gmail.com"
- **PORT** = 587
- **LOGIN** = "email@gmail.com" # Your email
- **PASSWORD** = "**\*\***" # Your password
- **SEND_EMAIL** = "email@enline-transmission.com"
- **PATH_ARCHIVE** = "C:\data\path_example\data.xls"
- **FILENAME** = "FILENAME.extension"

## Fourth allow less app on google

It's necessary to allow app less secure apps on google account. See this video to do this: [video](https://www.youtube.com/watch?v=Ee7PDsbfOUI&ab_channel=wpguide)

# Fifth execute the script

With virtualenv enabled, packages installed and environment variables set, it's time to run the code.

```bash
python sendEmails.py
```
